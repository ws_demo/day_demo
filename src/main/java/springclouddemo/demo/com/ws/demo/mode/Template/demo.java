package springclouddemo.demo.com.ws.demo.mode.Template;

/**
 * @description: 模板方法
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/28
 */
public class demo {
    public static void main(String[] args) {
        AbstractTemplate t = new ConcreteTemplate();
        // 调用模板方法
        t.templateMethod();
    }
}
