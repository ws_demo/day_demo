package springclouddemo.demo.com.ws.demo.mode.Observer;

public abstract class Observer {
    protected Subject subject;

    public abstract void update();
}