package springclouddemo.demo.com.ws.demo.mode.factory.three;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class GuoBaoRouFactory implements MakeFoodFactory{
    /**
     * 选择原料
     *
     * @return
     */
    @Override
    public String chooseRawMaterial() {
        return "里脊肉";
    }

    /**
     * 选择厨具
     *
     * @return
     */
    @Override
    public String chooseKitchenware() {
        return "炒锅";
    }
}
