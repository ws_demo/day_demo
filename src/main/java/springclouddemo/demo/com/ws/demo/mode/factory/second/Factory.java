package springclouddemo.demo.com.ws.demo.mode.factory.second;

import springclouddemo.demo.com.ws.demo.mode.factory.first.Food;

/**
 * @description: 工厂模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public interface Factory {
    Food makeFood(String name);
}

