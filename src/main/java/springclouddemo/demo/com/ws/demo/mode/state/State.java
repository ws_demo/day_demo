package springclouddemo.demo.com.ws.demo.mode.state;

public interface State {
    void doAction(Context context);
}