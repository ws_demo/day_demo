package springclouddemo.demo.com.ws.demo.test;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/6/22
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestTest {
    String value() default "";
    String name() default "";
}
