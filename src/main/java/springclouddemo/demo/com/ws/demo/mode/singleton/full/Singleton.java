package springclouddemo.demo.com.ws.demo.mode.singleton.full;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class Singleton{

    // 首先，也是先堵死 new Singleton() 这条路
    private Singleton() {}
    /**
     *   和饿汉模式相比，这边不需要先实例化出来，注意这里的 volatile，它是必须的
     *   确保变量能偶时时更新
     */

    private static volatile Singleton instance = null;

    public static Singleton getInstance() {
        if (instance == null) {
            // 加锁
            synchronized (Singleton.class) {
                // 这一次判断也是必须的，不然会有并发问题
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
