package springclouddemo.demo.com.ws.demo.test;

import lombok.Data;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/6/17
 */
@Data
public class Student {
    private String name;
}
