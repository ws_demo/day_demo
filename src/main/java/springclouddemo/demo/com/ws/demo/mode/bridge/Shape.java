package springclouddemo.demo.com.ws.demo.mode.bridge;

public abstract class Shape {
    protected FoodInterface foodInterface;

    protected Shape(FoodInterface foodInterface) {
        this.foodInterface = foodInterface;
    }

    public abstract void setFoodInterface();
}