package springclouddemo.demo.com.ws.demo.mode.Strategy;

public interface Strategy {
    int doOperation(int num1, int num2);
}