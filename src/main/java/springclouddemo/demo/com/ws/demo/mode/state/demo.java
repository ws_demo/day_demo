package springclouddemo.demo.com.ws.demo.mode.state;

/**
 * @description: 状态模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/28
 */
public class demo {
    public static void main(String[] args) {
        // 我们需要操作的是 iPhone X
        Context context = new Context("iPhone 13");

        // 看看怎么进行补库存操作
        State revertState = new RevertState();
        revertState.doAction(context);
// 如果需要我们可以获取当前的状态
        System.out.println(context.getState().toString());
        // 同样的，减库存操作也非常简单
        State deductState = new DeductState();
        deductState.doAction(context);

        // 如果需要我们可以获取当前的状态
        System.out.println(context.getState().toString());
    }
}
