package springclouddemo.demo.com.ws.demo.mode.factory.three;

/**
 * @description: 抽象工厂模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public interface MakeFoodFactory {
    /**
     * 选择原料
     * @return
     */
    String chooseRawMaterial();
    /**
     * 选择厨具
     * @return
     */
    String chooseKitchenware();
}

