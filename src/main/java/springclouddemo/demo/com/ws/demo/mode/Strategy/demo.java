package springclouddemo.demo.com.ws.demo.mode.Strategy;

/**
 * 策略者模式
 * @author shuai
 */
public class demo {
   public static void main(String[] args) {

      //很像桥接模式
      Context context = new Context(new OperationAdd());    
      System.out.println("10 + 5 = " + context.executeStrategy(10, 5));
 
      context = new Context(new OperationSubtract());      
      System.out.println("10 - 5 = " + context.executeStrategy(10, 5));
 
      context = new Context(new OperationMultiply());    
      System.out.println("10 * 5 = " + context.executeStrategy(10, 5));
   }
}