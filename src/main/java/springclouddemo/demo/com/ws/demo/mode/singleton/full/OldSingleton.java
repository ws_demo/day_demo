package springclouddemo.demo.com.ws.demo.mode.singleton.full;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class OldSingleton {
    private static OldSingleton singleton;

    private OldSingleton() {

    }

    public static synchronized OldSingleton getInstance() {
        if (singleton == null) {
            singleton = new OldSingleton();
        }
        return singleton;
    }
}
