package springclouddemo.demo.com.ws.demo.mode.factory;

import springclouddemo.demo.com.ws.demo.mode.factory.three.BunFactory;
import springclouddemo.demo.com.ws.demo.mode.factory.three.MakeFoodFactory;

/**
 * @description: 工厂模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class demo {
    public static void main(String[] args) {
//        // 先选择一个具体的工厂
//        Factory factory = new ChineseFoodFactory();
//        // 由第一步的工厂产生具体的对象，不同的工厂造出不一样的对象
//        Food food = factory.makeFood("A");


        MakeFoodFactory factory = new BunFactory();
        System.out.println("使用" + factory.chooseKitchenware() + "，用" + factory.chooseRawMaterial() + "蒸包子");
    }
}
