package springclouddemo.demo.com.ws.demo.mode.factory.second;

import springclouddemo.demo.com.ws.demo.mode.factory.first.Food;

public class ChineseFoodFactory implements Factory {

    @Override
    public Food makeFood(String name) {
        if (name.equals("A")) {
            System.out.println("ChineseFoodA");
            return new ChineseFoodA();
        } else if (name.equals("B")) {
            System.out.println("ChineseFoodB");
            return new ChineseFoodB();
        } else {
            return null;
        }
    }
}
