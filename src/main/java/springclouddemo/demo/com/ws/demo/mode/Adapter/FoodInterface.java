package springclouddemo.demo.com.ws.demo.mode.Adapter;

/**
 * @description: 类适配者模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public interface FoodInterface {

    void makeFood();

    void eatFood();

    void lookFood();

}
