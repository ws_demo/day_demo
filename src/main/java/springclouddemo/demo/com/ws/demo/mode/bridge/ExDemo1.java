package springclouddemo.demo.com.ws.demo.mode.bridge;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class ExDemo1 extends Shape{


    protected ExDemo1(FoodInterface foodInterface) {
        super(foodInterface);
    }

    @Override
    public void setFoodInterface() {
        foodInterface.makeFood();
        System.out.println("ExDemo1");
    }
}
