package springclouddemo.demo.com.ws.demo.mode.decorate;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @description: 装饰器模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class demo {
    public static void main(String[] args) throws FileNotFoundException {
        //最近大街上流行起来了“一点点”，我们把饮料分为三类：
        // 红茶、绿茶、咖啡，在这三大类的基础上，又增加了许多的口味，什么金桔柠檬红茶、
        // 金桔柠檬珍珠绿茶、芒果红茶、芒果绿茶、芒果珍珠红茶、烤珍珠红茶、烤珍珠芒果绿茶、
        // 椰香胚芽咖啡、焦糖可可咖啡等等，每家店都有很长的菜单，但是仔细看下，其实原料也没几样，
        // 但是可以搭配出很多组合，如果顾客需要，很多没出现在菜单中的饮料他们也是可以做的。
        //
        //在这个例子中，红茶、绿茶、咖啡是最基础的饮料，其他的像金桔柠檬、芒果、珍珠、椰果、
        // 焦糖等都属于装饰用的。当然，在开发中，我们确实可以像门店一样，开发这些类：LemonBlackTea、LemonGreenTea
        // 但是，很快我们就发现，这样子干肯定是不行的，这会导致我们需要组合出所有的可能，而且如果客人需要在红茶中加双份柠檬怎么办？三份柠檬怎么办？


        // 首先，我们需要一个基础饮料，红茶、绿茶或咖啡
        Beverage beverage = new GreenTea();
        Beverage beverage2 = new BlackTea();

        // 开始装饰
        beverage = new Lemon(beverage); // 先加一份柠檬
        beverage = new Mango(beverage); // 再加一份芒果

        System.out.println(beverage.getDescription() + " 价格：￥" + beverage.cost());
        //"绿茶, 加柠檬, 加芒果 价格：￥16"

        // 开始装饰
        beverage2 = new Lemon(beverage2); // 先加一份柠檬
        beverage2 = new Mango(beverage2); // 再加一份芒果
        beverage2 = new Mango(beverage2); // 再加一份芒果

        System.out.println(beverage2.getDescription() + " 价格：￥" + beverage2.cost());

        DataInputStream is = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("")
                ));


    }
}
