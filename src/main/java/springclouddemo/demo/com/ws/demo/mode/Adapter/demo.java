package springclouddemo.demo.com.ws.demo.mode.Adapter;

import springclouddemo.demo.com.ws.demo.mode.agent.FoodService;
import springclouddemo.demo.com.ws.demo.mode.agent.FoodServiceImpl;
import springclouddemo.demo.com.ws.demo.mode.agent.InvocationHandlerImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @description: 对象适配器模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class demo {
    public static void main(String[] args) {
        // 有一只野鸡
         Cock wildCock = new WildCock();
        // 成功将野鸡适配成鸭
         Duck duck = new CockAdapter(wildCock);
        duck.quack();
    }
}
