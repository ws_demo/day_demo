package springclouddemo.demo.com.ws.demo.mode.bridge;

import springclouddemo.demo.com.ws.demo.mode.agent.FoodService;
import springclouddemo.demo.com.ws.demo.mode.agent.FoodServiceImpl;
import springclouddemo.demo.com.ws.demo.mode.agent.InvocationHandlerImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @description: 桥接模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class demo {
    public static void main(String[] args) {
        Shape exDemo1 = new ExDemo1(new FoodInterfaceImpl());
        Shape exDemo2 = new ExDemo2(new Food2InterfaceImpl());

        Shape exDemo3 = new ExDemo1(new Food3InterfaceImpl());
        exDemo1.setFoodInterface();
        exDemo2.setFoodInterface();
        exDemo3.setFoodInterface();


    }
}
