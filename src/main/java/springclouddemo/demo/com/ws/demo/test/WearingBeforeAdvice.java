package springclouddemo.demo.com.ws.demo.test;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/6/22
 */
@Component
@Aspect
public class WearingBeforeAdvice  {
    @Pointcut(value = "@annotation(springclouddemo.demo.com.ws.demo.test.RequestTest)")
    public void cut(){
    }

    @Before("@annotation(requestTest)")
    public void before(JoinPoint joinPoint,RequestTest requestTest){
        System.out.println("-------------------------------before"+ Arrays.toString(joinPoint.getArgs()) +requestTest);
    }
    @After(value = "cut()")
    public void after(){
        System.out.println("-------------------------------after");
    }

    @Around("cut()")
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        System.out.println("-------------------------------Around"+ Arrays.toString(proceedingJoinPoint.getArgs()));
        Object result = proceedingJoinPoint.proceed();
        System.out.println("result"+result.toString());
    }
    @AfterReturning(returning = "res", pointcut = "cut()")
    public void doAfterReturning(Object res) throws Throwable {
        // 处理完请求，返回内容
        System.out.println("方法的返回值 : " + res);
    }

    /**
     * 异常抛出增强，相当于ThrowsAdvice
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(throwing = "exception", pointcut = "cut()")
    public void throes(JoinPoint joinPoint, Exception exception){
        System.out.println("方法异常时执行....."+exception.getMessage());
    }
}
