package springclouddemo.demo.com.ws.demo.mode.state;

import lombok.Data;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/28
 */
@Data
public class Context {
    private State state;
    private String name;

    public Context(String name) {
        this.name = name;
    }
}
