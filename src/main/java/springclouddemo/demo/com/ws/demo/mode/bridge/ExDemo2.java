package springclouddemo.demo.com.ws.demo.mode.bridge;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class ExDemo2 extends Shape{


    protected ExDemo2(FoodInterface foodInterface) {
        super(foodInterface);
    }

    @Override
    public void setFoodInterface() {
        foodInterface.makeFood();
        System.out.println("ExDemo2");
    }
}
