package springclouddemo.demo.com.ws.demo.mode.builder;

/**
 * @description: 建造者模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class demo {
    public static void main(String[] args) {
        System.out.println(User.builder().age(1).build());
    }
}
