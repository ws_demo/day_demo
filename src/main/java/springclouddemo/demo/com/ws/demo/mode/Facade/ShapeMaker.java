package springclouddemo.demo.com.ws.demo.mode.Facade;

import springclouddemo.demo.com.ws.demo.mode.bridge.Food2InterfaceImpl;
import springclouddemo.demo.com.ws.demo.mode.bridge.Food3InterfaceImpl;
import springclouddemo.demo.com.ws.demo.mode.bridge.FoodInterface;
import springclouddemo.demo.com.ws.demo.mode.bridge.FoodInterfaceImpl;

/**
 * @description: 门面模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class ShapeMaker {
    private FoodInterface foodInterface1;
    private FoodInterface foodInterface2;
    private FoodInterface foodInterface3;


    public void ShapeMaker() {
        foodInterface1 = new FoodInterfaceImpl();
        foodInterface2 = new Food2InterfaceImpl();
        foodInterface3 = new Food3InterfaceImpl();
    }

    public void Demo1() {
        foodInterface1.makeFood();
    }

    public void Demo2() {
        foodInterface2.makeFood();
    }

    public void Demo3() {
        foodInterface3.makeFood();
    }


}
