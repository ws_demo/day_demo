package springclouddemo.demo.com.ws.demo.mode.chainOfResponsibility;

/**
 * @description: 责任链模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/28
 */
public class demo {
    public static void main(String[] args) {
        RuleHandler newUserHandler = new NewUserRuleHandler();
        RuleHandler locationHandler = new LocationRuleHandler();
        RuleHandler limitHandler = new LimitRuleHandler();
        newUserHandler.setSuccessor(locationHandler);
        // 假设本次活动仅校验地区和奖品数量，不校验新老用户
        locationHandler.setSuccessor(limitHandler);

        //地区不符合
//        locationHandler.apply(new Context());
        //不是新用户
        newUserHandler.apply(new Context());
    }
}
