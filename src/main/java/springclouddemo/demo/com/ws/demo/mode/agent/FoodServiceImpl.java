package springclouddemo.demo.com.ws.demo.mode.agent;

import springclouddemo.demo.com.ws.demo.mode.factory.first.Food;
import springclouddemo.demo.com.ws.demo.mode.factory.second.AmericanFoodA;
import springclouddemo.demo.com.ws.demo.mode.factory.second.AmericanFoodB;

public class FoodServiceImpl implements FoodService {
    @Override
    public Food foodA() {
        System.out.println("我被调用了");
        Food f = new AmericanFoodA();
        return f;
    }

    @Override
    public Food foodB() {
        Food f = new AmericanFoodB();
        return f;
    }
}
