package springclouddemo.demo.com.ws.demo.mode.Adapter;

/**
 * @description: 类适配者模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class FoodInterImpl extends FoodInter implements FoodInterface {


    @Override
    public void lookFood() {

    }
}
