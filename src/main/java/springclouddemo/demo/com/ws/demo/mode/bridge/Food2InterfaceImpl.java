package springclouddemo.demo.com.ws.demo.mode.bridge;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class Food2InterfaceImpl implements FoodInterface {

    @Override
    public void makeFood() {
        System.out.println(2);
    }
}
