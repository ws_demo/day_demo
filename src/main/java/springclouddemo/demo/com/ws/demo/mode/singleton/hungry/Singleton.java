package springclouddemo.demo.com.ws.demo.mode.singleton.hungry;

/**
 * @description:
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class Singleton {
    // 首先，将 new Singleton() 堵死
    private Singleton() {};
    /**
     *  创建私有静态实例，意味着这个类第一次使用的时候就会进行创建
      */

    private static Singleton instance = new Singleton();

    public static Singleton getInstance() {
        return instance;
    }

    public  String getDemo(String mode) {return "demo";}
}
