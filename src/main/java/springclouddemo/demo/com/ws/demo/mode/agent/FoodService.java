package springclouddemo.demo.com.ws.demo.mode.agent;

import springclouddemo.demo.com.ws.demo.mode.factory.first.Food;
import springclouddemo.demo.com.ws.demo.mode.factory.second.AmericanFoodA;
import springclouddemo.demo.com.ws.demo.mode.factory.second.AmericanFoodB;

public interface FoodService {
    Food foodA();
    Food foodB();
}

