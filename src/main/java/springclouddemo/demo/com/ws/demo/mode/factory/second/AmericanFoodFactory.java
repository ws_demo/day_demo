package springclouddemo.demo.com.ws.demo.mode.factory.second;

import springclouddemo.demo.com.ws.demo.mode.factory.first.Food;

public class AmericanFoodFactory implements Factory {

    @Override
    public Food makeFood(String name) {
        if (name.equals("A")) {
            System.out.println("AmericanFoodA");
            return new AmericanFoodA();
        } else if (name.equals("B")) {
            System.out.println("AmericanFoodB");
            return new AmericanFoodB();
        } else {
            return null;
        }
    }
}
