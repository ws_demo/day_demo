package springclouddemo.demo.com.ws.demo.mode.agent;

import javax.security.auth.Subject;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @description: 代理模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class demo {
    public static void main(String[] args) {
        //自定义代理
//        FoodService foodService = new FoodServiceProxy();
//        foodService.foodA();

        //jdk动态代理
        FoodService foodService = new FoodServiceImpl();
        InvocationHandler handler = new InvocationHandlerImpl(foodService);
        ClassLoader loader = foodService.getClass().getClassLoader();
        Class[] interfaces = foodService.getClass().getInterfaces();
        FoodService foodService1 = (FoodService) Proxy.newProxyInstance(loader, interfaces, handler);
        foodService1.foodA();

    }
}
