package springclouddemo.demo.com.ws.demo.mode.factory.first;

/**
 * @description: 简单工厂模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class FoodFactory {
    public static Food makeFood(String name) {
        if (name.equals("noodle")) {
            Food noodle = new LanZhouNoodle();
            return noodle;
        } else if (name.equals("chicken")) {
            Food chicken = new HuangMenChicken();
            return chicken;
        } else {
            return null;
        }
    }
}
