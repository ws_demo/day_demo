package springclouddemo.demo.com.ws.demo.mode.chainOfResponsibility;

public class LimitRuleHandler extends RuleHandler {
    @Override
    public void apply(Context context) {
        int remainedTimes = Math.random()>0.5?1:0;
        if (remainedTimes > 0) {
            if (this.getSuccessor() != null) {
                this.getSuccessor().apply(context);
            }
        } else {
            throw new RuntimeException("您来得太晚了，奖品被领完了");
        }
    }
}