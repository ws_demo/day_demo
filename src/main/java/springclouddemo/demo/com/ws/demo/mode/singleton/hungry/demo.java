package springclouddemo.demo.com.ws.demo.mode.singleton.hungry;

/**
 * @description: 单例模式
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/22
 */
public class demo {
    public static void main(String[] args) {
        //饿汉式
        System.out.println(Singleton.getInstance().getDemo("ss"));
    }
}
