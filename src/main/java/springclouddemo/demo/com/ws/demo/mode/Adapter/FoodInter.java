package springclouddemo.demo.com.ws.demo.mode.Adapter;

/**
 * @description: 对象适配者
 * @version: 1.0
 * @author: shuai.wang07@hand-china.com
 * @Date: 2021/7/27
 */
public class FoodInter {

    public void makeFood() {

    }

    public void eatFood() {
    }

}
